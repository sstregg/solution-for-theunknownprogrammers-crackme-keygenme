﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Crackme_KeyGen
{
    class AssemblyDecrypter
    {
        public static Dictionary<int, string> field_77 = new Dictionary<int, string>();

        public static void method_80 ( Stream resource )
        {
            Stream stream1;
            MemoryStream stream2;
            byte[] numArray1;
            int num1;
            BinaryReader reader1;
            int num2;
            int num3;
            int num4;
            int num5;
            byte[] numArray2;
            bool bool1;
            try
            {
                stream1 = resource;//Type.GetTypeFromHandle( Class_13 ).Assembly.GetManifestResourceStream( "\u200c.resources" );
                try
                {
                    stream2 = new MemoryStream();
                    try
                    {
                        numArray1 = new byte[0x1000];
                        num1 = stream1.Read( numArray1, 0x0, 0x1000 );
                        do
                        {
                            stream2.Write( numArray1, 0x0, num1 );
                            num1 = stream1.Read( numArray1, 0x0, 0x1000 );
                        } while ( num1 != 0x0 );
                        reader1 = new BinaryReader( new MemoryStream( Class_12.method_75( stream2.ToArray() ) ) );
                        try
                        {
                            reader1.BaseStream.Seek( 0x0, SeekOrigin.Begin );
                            num2 = reader1.ReadInt32();
                            num3 = 0x0;

                            while ( !(num3 > num2) )
                            {
                                num4 = reader1.ReadInt32();
                                num5 = reader1.ReadInt32();
                                numArray2 = reader1.ReadBytes( num5 );
                                field_77.Add( num4, Encoding.ASCII.GetString( numArray2 ) );
                                num3 += 0x1;
                            }
                        }
                        finally
                        {
                            bool1 = (reader1 == null);
                            if ( !bool1 )
                            {
                                reader1.Dispose();
                            }
                        }
                    }
                    finally
                    {
                        bool1 = (stream2 == null);
                        if ( !bool1 )
                        {
                            stream2.Dispose();
                        }
                    }
                }
                finally
                {
                    bool1 = (stream1 == null);
                    if ( !bool1 )
                    {
                        stream1.Dispose();
                    }
                }
            }
            catch
            {
                Environment.FailFast( new InvalidProgramException().Message );
            }
        }

    }

    class StringDecrypter
    {
        private static ushort method_83 ( ushort param_226 )
        {
            return (ushort)(((param_226 & 0x8000) >> 0xF) | (param_226 << 0x1));
        }

        public static string method_82 ( string param_61, string param_62 )
        {
            StringBuilder builder1;
            int num1;
            ushort num2;
            string str1;
            bool bool1;
            builder1 = new StringBuilder();
            for ( num1 = 0x0; (num1 < param_61.Length); num1 += 0x4 )
            {
                num2 = ushort.Parse( param_61.Substring( num1, 0x4 ), NumberStyles.AllowHexSpecifier );
                num2 = method_83( num2 );
                builder1.Append( (char)(num2 ^ param_62[(num1 / 4) % param_62.Length]) );
            }
            str1 = builder1.ToString();
            return str1;
        }
    }

    public class Class_02
    {
        public uint field_02 = 0x146;
        public uint field_03 = 0x283;
        public uint field_04=0x6;
    }

    public class ReversedAlgo
    {
        private static object[] method_20 ( Stack<object> param_19, int param_20 )
        {
            object[] objArray1;
            int num1;
            object[] objArray2;
            bool bool1;
            objArray1 = new object[param_20];
            for ( num1 = 0x0; (num1 < param_20); num1 += 0x1 )
            {
                objArray1[num1] = param_19.Pop();
            }
            objArray2 = objArray1;
            return objArray2;
        }


        private static object[] method_19 ( MethodBase param_17, Stack<object> param_18 )
        {
            ParameterInfo[] infoArray1;
            object[] objArray1;
            object[] objArray2;
            infoArray1 = param_17.GetParameters();
            objArray1 = method_20( param_18, infoArray1.Length );
            objArray2 = Enumerable.ToArray<object>( Enumerable.Reverse<object>( objArray1 ) );
            return objArray2;
        }

        public enum Class_07
        {
            field_31,
            field_32,
            field_33,
            field_34,
            field_35,
            field_36,
            field_37,
            field_38,
            field_39 = 4096,
            field_40,
            field_41,
            field_42,
            field_43,
            field_44,
            field_45,
            field_46,
            field_47,
            field_48,
            field_49
        }

        // Class_05
        public static void method_18 ( BinaryReader param_12, uint param_13, Class_02 param_14, Dictionary<uint, object> param_15, object[] param_16 )
        {
            param_12.BaseStream.Seek( (long)((ulong)param_13), SeekOrigin.Begin );
            Stack<object> stack = new Stack<object>();
            if ( param_16 != null )
            {
                param_16 = param_16.Reverse<object>().ToArray<object>();
                object[] array = param_16;
                for ( int i = 0; i < array.Length; i++ )
                {
                    object item = array[i];
                    stack.Push( item );
                }
            }
            object[] array2 = new object[param_14.field_04];
            bool flag = true;
            while ( flag )
            {
                Class_07 class_ = (Class_07)param_12.ReadUInt16();
                uint num = 0u;
                if ( (ushort)class_ > 4096 )
                {
                    num = param_12.ReadUInt32();
                }
                Class_07 class_2 = class_;
                switch ( class_2 )
                {
                case Class_07.field_32:
                    flag = false;
                    break;
                case Class_07.field_33:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) + Convert.ToInt32( array3[1] ) );
                        break;
                    }
                case Class_07.field_34:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) - Convert.ToInt32( array3[1] ) );
                        break;
                    }
                case Class_07.field_35:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) * Convert.ToInt32( array3[1] ) );
                        break;
                    }
                case Class_07.field_36:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) / Convert.ToInt32( array3[1] ) );
                        break;
                    }
                case Class_07.field_37:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) < Convert.ToInt32( array3[1] ) );
                        break;
                    }
                case Class_07.field_38:
                    {
                        object[] array3 = method_20( stack, 2 );
                        stack.Push( Convert.ToInt32( array3[0] ) > Convert.ToInt32( array3[1] ) );
                        break;
                    }
                default:
                    switch ( class_2 )
                    {
                    case Class_07.field_40:
                        stack.Push( param_15[num] );
                        break;
                    case Class_07.field_41:
                        stack.Pop();
                        break;
                    case Class_07.field_42:
                        {
                            MethodInfo methodInfo = (MethodInfo)param_15[num];
                            bool flag2 = methodInfo.ReturnType.FullName != "System.Void";
                            object[] parameters = method_19( methodInfo, stack );
                            object obj = methodInfo.IsStatic ? null : stack.Pop();
                            object item2 = methodInfo.Invoke( obj, parameters );
                            if ( flag2 )
                            {
                                stack.Push( item2 );
                            }
                            break;
                        }
                    case Class_07.field_43:
                        param_12.BaseStream.Seek( (long)((ulong)num), SeekOrigin.Begin );
                        break;
                    case Class_07.field_44:
                        {
                            object[] array3 = method_20( stack, 2 );
                            if ( array3[0] == array3[1] )
                            {
                                param_12.BaseStream.Seek( (long)((ulong)num), SeekOrigin.Begin );
                            }
                            break;
                        }
                    case Class_07.field_45:
                        {
                            object[] array3 = method_20( stack, 2 );
                            if ( array3[0] == array3[1] )
                            {
                                param_12.BaseStream.Seek( (long)((ulong)num), SeekOrigin.Begin );
                            }
                            break;
                        }
                    case Class_07.field_46:
                        if ( (bool)stack.Pop() )
                        {
                            param_12.BaseStream.Seek( (long)((ulong)num), SeekOrigin.Begin );
                        }
                        break;
                    case Class_07.field_47:
                        if ( !(bool)stack.Pop() )
                        {
                            param_12.BaseStream.Seek( (long)((ulong)num), SeekOrigin.Begin );
                        }
                        break;
                    case Class_07.field_48:
                        stack.Push( array2[(int)((UIntPtr)num)] );
                        break;
                    case Class_07.field_49:
                        array2[(int)((UIntPtr)num)] = stack.Pop();
                        break;
                    }
                    break;
                }
            }
        }


    }

    class Class_12
    {
        private static int method_69 ( byte[] param_43 )
        {
            return (((param_43[0x0] & 0x2) == 0x2) ? 0x9 : 0x3);
        }


        internal static int method_70 ( byte[] param_44 )
        {
            int num1;
            bool bool1;
            bool1 = (!(Class_12.method_69( param_44 ) == 0x9));
            if ( !bool1 )
            {
                num1 = (((param_44[0x5] | (param_44[0x6] << 0x8)) | (param_44[0x7] << 0x10)) | (param_44[0x8] << 0x18));
            }
            else
            {
                num1 = param_44[0x2];
            }
            return num1;
        }


        public static byte[] method_75 ( byte[] param_225 )
        {
            int num1;
            int num2;
            int num3;
            int num4;
            uint num5;
            byte[] numArray1;
            int[] numArray2;
            byte[] numArray3;
            int num6;
            int num7;
            int num8;
            uint num9;
            byte[] numArray4;
            uint num10;
            uint num11;
            uint num12;
            int num13;
            int num14;
            byte[] numArray5;
            bool bool1;
            num2 = Class_12.method_70( param_225 );
            num3 = Class_12.method_69( param_225 );
            num4 = 0x0;
            num5 = 0x1;
            numArray1 = new byte[num2];
            numArray2 = new int[0x1000];
            numArray3 = new byte[0x1000];
            num6 = (((num2 - 0x6) - 0x4) - 0x1);
            num7 = unchecked( (int)0xFFFFFFFF );
            num9 = 0x0;
            num1 = ((param_225[0x0] >> 0x2) & 0x3);
            //(0x1 ? (num1 == 0x3) : 0x1)
            //bool1 = num1;
            if ( false )
            {
                throw new ArgumentException( "C# version only supports level 1 and 3" );
            }
            bool1 = ((param_225[0x0] & 0x1) == 0x1);
            if ( !bool1 )
            {
                numArray4 = new byte[num2];
                Array.Copy( param_225, Class_12.method_69( param_225 ), numArray4, 0x0, num2 );
                numArray5 = numArray4;
            }
            else
            {
                while ( true )
                {
                    bool1 = (!(num5 == 0x1));
                    if ( !bool1 )
                    {
                        num5 = (uint)(((param_225[num3] | (param_225[(num3 + 0x1)] << 0x8)) | (param_225[(num3 + 0x2)] << 0x10)) | (param_225[(num3 + 0x3)] << 0x18));
                        num3 += 0x4;
                        bool1 = (num4 > num6);
                        if ( !bool1 )
                        {
                            bool1 = (!(num1 == 0x1));
                            if ( !bool1 )
                            {
                                num9 = (uint)((param_225[num3] | (param_225[(num3 + 0x1)] << 0x8)) | (param_225[(num3 + 0x2)] << 0x10));
                            }
                            else
                            {
                                num9 = (uint)(((param_225[num3] | (param_225[(num3 + 0x1)] << 0x8)) | (param_225[(num3 + 0x2)] << 0x10)) | (param_225[(num3 + 0x3)] << 0x18));
                            }
                        }
                    }
                    bool1 = (!((num5 & 0x1) == 0x1));
                    if ( !bool1 )
                    {
                        num5 >>= 0x1;
                        bool1 = (!(num1 == 0x1));
                        if ( !bool1 )
                        {
                            num8 = (int)((num9 >> 0x4) & 0xFFF);
                            num11 = (uint)numArray2[num8];
                            bool1 = ((num9 & 0xF) == 0x0);
                            if ( !bool1 )
                            {
                                num10 = ((num9 & 0xF) + 0x2);
                                num3 += 0x2;
                            }
                            else
                            {
                                num10 = param_225[(num3 + 0x2)];
                                num3 += 0x3;
                            }
                        }
                        else
                        {
                            bool1 = (!((num9 & 0x3) == 0x0));
                            if ( !bool1 )
                            {
                                num12 = ((num9 & 0xFF) >> 0x2);
                                num10 = 0x3;
                                num3 += 0x1;
                            }
                            else
                            {
                                bool1 = (!((num9 & 0x2) == 0x0));
                                if ( !bool1 )
                                {
                                    num12 = ((num9 & 0xFFFF) >> 0x2);
                                    num10 = 0x3;
                                    num3 += 0x2;
                                }
                                else
                                {
                                    bool1 = (!((num9 & 0x1) == 0x0));
                                    if ( !bool1 )
                                    {
                                        num12 = ((num9 & 0xFFFF) >> 0x6);
                                        num10 = (((num9 >> 0x2) & 0xF) + 0x3);
                                        num3 += 0x2;
                                    }
                                    else
                                    {
                                        bool1 = ((num9 & 0x7F) == 0x3);
                                        if ( !bool1 )
                                        {
                                            num12 = ((num9 >> 0x7) & 0x1FFFF);
                                            num10 = (((num9 >> 0x2) & 0x1F) + 0x2);
                                            num3 += 0x3;
                                        }
                                        else
                                        {
                                            num12 = (num9 >> 0xF);
                                            num10 = (((num9 >> 0x7) & 0xFF) + 0x3);
                                            num3 += 0x4;
                                        }
                                    }
                                }
                            }
                            num11 = (uint)(num4 - num12);
                        }
                        numArray1[num4] = numArray1[num11];
                        numArray1[(num4 + 0x1)] = numArray1[(num11 + 0x1)];
                        numArray1[(num4 + 0x2)] = numArray1[(num11 + 0x2)];
                        for ( num13 = 0x3; (num13 < num10); num13++ )
                        {
                            numArray1[(num4 + num13)] = numArray1[(num11 + num13)];
                        }
                        num4 += (int)num10;
                        bool1 = (!(num1 == 0x1));
                        if ( !bool1 )
                        {
                            num9 = (uint)((numArray1[(num7 + 0x1)] | (numArray1[(num7 + 0x2)] << 0x8)) | (numArray1[(num7 + 0x3)] << 0x10));
                            while ( (num7 < (num4 - num10)) )
                            {
                                num7 += 0x1;
                                num8 = (int)(((num9 >> 0xC) ^ num9) & 0xFFF);
                                numArray2[num8] = num7;
                                numArray3[num8] = 0x1;
                                num9 = (uint)(((num9 >> 0x8) & 0xFFFF) | (numArray1[(num7 + 0x3)] << 0x10));
                            }
                            num9 = (uint)((param_225[num3] | (param_225[(num3 + 0x1)] << 0x8)) | (param_225[(num3 + 0x2)] << 0x10));
                        }
                        else
                        {
                            num9 = (uint)(((param_225[num3] | (param_225[(num3 + 0x1)] << 0x8)) | (param_225[(num3 + 0x2)] << 0x10)) | (param_225[(num3 + 0x3)] << 0x18));
                        }
                        num7 = (num4 - 0x1);
                    }
                    else
                    {
                        bool1 = (num4 > num6);
                        if ( !bool1 )
                        {
                            numArray1[num4] = param_225[num3];
                            num4 += 0x1;
                            num3 += 0x1;
                            num5 >>= 0x1;
                            bool1 = (!(num1 == 0x1));
                            if ( !bool1 )
                            {
                                while ( (num7 < (num4 - 0x3)) )
                                {
                                    num7 += 0x1;
                                    num14 = ((numArray1[num7] | (numArray1[(num7 + 0x1)] << 0x8)) | (numArray1[(num7 + 0x2)] << 0x10));
                                    num8 = (((num14 >> 0xC) ^ num14) & 0xFFF);
                                    numArray2[num8] = num7;
                                    numArray3[num8] = 0x1;
                                }
                                num9 = (uint)(((num9 >> 0x8) & 0xFFFF) | (param_225[(num3 + 0x2)] << 0x10));
                            }
                            else
                            {
                                num9 = (uint)((((num9 >> 0x8) & 0xFFFF) | (param_225[(num3 + 0x2)] << 0x10)) | (param_225[(num3 + 0x3)] << 0x18));
                            }
                        }
                        else
                        {
                            while ( (!(num4 > (num2 - 0x1))) )
                            {
                                bool1 = (!(num5 == 0x1));
                                if ( !bool1 )
                                {
                                    num3 += 0x4;
                                    num5 = 0x80000000;
                                }
                                numArray1[num4] = param_225[num3];
                                num4 += 0x1;
                                num3 += 0x1;
                                num5 >>= 0x1;
                            }
                            numArray5 = numArray1;
                            break;
                        }
                    }
                    bool1 = true;
                }
            }
            return numArray5;
        }

    }
}
