﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using dnlib.DotNet.Writer;
using dnlib.PE;
using MethodBody = dnlib.DotNet.Emit.MethodBody;

namespace Crackme_KeyGen
{
    public class CodeDecrypter : IDisposable
    {
        private Stream mFileStream;

        public CodeDecrypter ( ) { }

        public CodeDecrypter ( string file )
        {
            mFileStream = File.Open( file, FileMode.Open, FileAccess.ReadWrite );
        }

        public void Clean ( long codeOffset, long sizeOffset = 0x0 )
        {
            var mWriter = new BinaryWriter( mFileStream );
            var mReader = new BinaryReader( mFileStream );

            patch_rw( mWriter, mReader );

            // if size offset is zero then get value from codeOffset - 8
            if ( sizeOffset == 0x0 )
                sizeOffset = codeOffset - 8;

            mFileStream.Position = sizeOffset;
            int codeSize = mReader.ReadInt32();
            mFileStream.Position = codeOffset;
            var code = mReader.ReadBytes( codeSize );

            code = DecryptCode( code );
            codeSize = code.Length;

            mFileStream.Position = sizeOffset;
            mWriter.Write( codeSize );

            mFileStream.Position = codeOffset;
            mWriter.Write( code );
            mWriter.Write( 0xDEADC0DE );

            mFileStream.Flush();
        }

        private void patch_rw ( BinaryWriter bw, BinaryReader br )
        {
            Debug.WriteLine( typeof( BinaryWriter )
                .GetField( "_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .GetValue( bw ) );
            Debug.WriteLine( typeof( BinaryReader )
                .GetField( "m_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .GetValue( br ) );

            typeof( BinaryWriter )
                .GetField( "_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .SetValue( bw, true );
            typeof( BinaryReader )
                .GetField( "m_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .SetValue( br, true );

            Debug.WriteLine( typeof( BinaryWriter )
                .GetField( "_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .GetValue( bw ) );
            Debug.WriteLine( typeof( BinaryReader )
                .GetField( "m_leaveOpen", BindingFlags.Instance | BindingFlags.NonPublic )
                .GetValue( br ) );
        }

        public byte[] DecryptCode ( byte[] byteCode )
        {
            if ( byteCode == null )
                throw new Exception( "Invalid byte code!" );
            if ( byteCode.Length < 4 )
                return byteCode;

            var temp = new byte[4]
            {
                byteCode[byteCode.Length - 4],
                byteCode[byteCode.Length - 3],
                byteCode[byteCode.Length - 2],
                byteCode[byteCode.Length - 1]
            };
            var id = BitConverter.ToUInt32( temp, 0 );

            if ( id != 0x1337C0DE )
                return byteCode;

            var code = new byte[byteCode.Length - 4];

            for ( int i = 0; i < code.Length; i++ )
            {
                code[i] = DecryptByteCode( byteCode[i] );
            }

            return code;
        }

        private static byte DecryptByteCode ( byte codeByte )
        {
            return (byte)(((codeByte & 0x1) << 0x7) | (codeByte >> 0x1));
        }

        public void Dispose ( )
        {
            if ( mFileStream != null )
            {
                mFileStream.Dispose();
                mFileStream = null;
            }
        }
    }

    class MethodDecrypter : IMethodDecrypter
    {
        private ModuleDefMD Module;

        public MethodDecrypter ( ModuleDefMD m )
        {
            Module = m;
        }

        public bool HasMethodBody ( uint rid )
        {
            return true;
        }

        public MethodBody GetMethodBody ( uint rid, RVA rva, IList<Parameter> parameters )
        {
            var body = Module.ReadCilBody( parameters, rva );

            if ( !body.HasInstructions )
                return body;

            var code = new CodeDecrypter().DecryptCode( body.RawBytes );
            if ( code.Length != body.RawBytes.Length )
                return MethodBodyReader.CreateCilBody( Module, code, null, parameters, body.Flags, body.MaxStack, (uint)code.Length, body.LocalVarSigTok );

            return body;
        }
    }

    class DelegateKiller
    {
        public static DelegateKiller Kill ( ModuleDefMD module )
        {
            return new DelegateKiller( module );
        }

        private DelegateKiller ( ModuleDefMD m )
        {
            m.MethodDecrypter = new MethodDecrypter( m );

            MethodDef method = null;
            uint rid = 1;

            do
            {
                method = m.ResolveMethod( rid++ );

                if ( method != null && method.HasBody )
                {
                    var rva = method.RVA;

                    if ( ((uint)rva & 0xFF000000) != 0 || method.Body.Instructions.Count == 0 )
                    {
                        method.RVA = 0;
                        continue;
                    }

                    var cil = method.Body.Instructions;
                    PatchInstructions( cil );
                    cil.UpdateInstructionOffsets();
                }

            } while ( method != null );
        }

        void PatchInstructions ( IList<Instruction> cil )
        {
            for ( int i = 0; i < cil.Count; i++ )
            {
                if ( cil[i].OpCode == OpCodes.Call )
                {
                    var md = cil[i].Operand as MethodDef;

                    if ( md == null )
                        continue;

                    if ( md.DeclaringType.BaseType.Name != "MulticastDelegate" )
                        continue;

                    var cctor = md.DeclaringType
                        .FindMethods( ".cctor" )
                        .FirstOrDefault();

                    if ( cctor == null )
                        continue;

                    var obj = cctor.Body.Instructions
                        .Where( instruction => instruction.OpCode == OpCodes.Ldtoken )
                        .ElementAtOrDefault( 1 );

                    if ( obj == null )
                        continue;

                    var operand = obj.Operand as MemberRef;

                    Instruction il = null;
                    if ( operand.Name == ".ctor" )
                        il = Instruction.Create( OpCodes.Newobj, operand );
                    else
                        il = Instruction.Create( operand.HasThis ? OpCodes.Callvirt : OpCodes.Call, operand );

                    cil[i] = il;
                }
            }
        }
    }

    class Program
    {
        [STAThread]
        static void Main ( string[] args )
        {
            // Date: 30 Nov 2013
            // Keygen for TheUnknownProgrammer's Crackme/Keygenme: http://crackmes.de/users/theunknownprogrammer/crackmekeygenme
            // Code is too bad :( Decrypted VM code stored at AssemblyDecrypter.cs
            // Tools which were used: SAE, DotNet Resolver, CFF Explorer, Patched dnlib, WinDbg
            // Spent time: 4 days
            // Author: SSTREGG aka NickoTin

            Console.Write( "Enter your name: " );
            Console.WriteLine( "Key: " + KeyGen( Console.ReadLine() ) );
            Console.ReadLine();

            //using ( var file = File.Open( @"Z:\4\3\Crackme\r\br.bin", FileMode.Open, FileAccess.Read ) )
            //{
            //    file.Position = 12;
            //    ReversedAlgo.method_18( new BinaryReader( file ), 12, new Class_02(), GetDict(),
            //        new object[] { "password", "nick" } );
            //}

            //using ( var file = File.Open( @"Z:\4\3\Crackme\r\method_80_res.bin", FileMode.Open, FileAccess.Read ) )
            //    AssemblyDecrypter.method_80( file );

            //string val=AssemblyDecrypter.field_77[unchecked( (int)0x5984A90D )];
            //var t= StringDecrypter.method_82( val, "⠂⠊⠁⠂⠇⠍⠁⠃⠎⠁⠃⠋⠇⠌⠌⠂⠆⠊⠇⠎" );
            //DelegateCleaner();
        }

        /// <summary>
        /// Decrypted key generation algo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static string KeyGen ( string name )
        {
            name = string.Concat( name.Reverse().Select( x => ((int)x).ToString( "X2" ) ) );
            return Convert.ToBase64String( Encoding.ASCII.GetBytes( name ) );
        }

        static Dictionary<uint, object> GetDict ( )
        {
            var dic = new Dictionary<uint, object>();

            dic.Add( 0x0, false );
            dic.Add( 0x2, true );
            dic.Add( 0x4, 0 );
            dic.Add( 0x9, AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault( x => x.FullName.Contains( "mscorlib" ) ) );
            dic.Add( 0x13, typeof( string ).GetMethod( "get_Length", BindingFlags.Instance | BindingFlags.Public ) );
            dic.Add( 0x32, "" );
            dic.Add( 0x34, 1 );
            dic.Add( 0x39, typeof( string ).GetMethod( "Substring", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof( int ), typeof( int ) }, null ) );
            dic.Add( 0x59, typeof( Convert ).GetMethod( "ToChar", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( string ) }, null ) );
            dic.Add( 0x76, typeof( Convert ).GetMethod( "ToInt32", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( char ) }, null ) );
            dic.Add( 0x94, "X2" );
            dic.Add( 0x98, typeof( int ).GetMethod( "ToString", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof( string ) }, null ) );
            dic.Add( 0xb5, typeof( string ).GetMethod( "Concat", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( string ), typeof( string ) }, null ) );
            dic.Add( 0xd2, 1 );
            dic.Add( 0xd7, typeof( Encoding ).GetMethod( "get_ASCII", BindingFlags.Static | BindingFlags.Public ) );
            dic.Add( 0xfc, typeof( Encoding ).GetMethod( "GetBytes", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof( string ) }, null ) );
            dic.Add( 0x121, typeof( Convert ).GetMethod( "ToBase64String", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( byte[] ) }, null ) );
            dic.Add( 0x146, typeof( string ).GetMethod( "Equals", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof( string ) }, null ) );
            dic.Add( 0x162, "WW91IGhhdmUgZW50ZXJlZCBhbiBpbnZhbGlkIHNlcmlhbCBrZXkuIFBsZWFzZSB0cnkgYWdhaW4u" );
            dic.Add( 0x1b0, typeof( Convert ).GetMethod( "FromBase64String", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( string ) }, null ) );
            dic.Add( 0x1d7, typeof( Encoding ).GetMethod( "GetString", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof( byte[] ) }, null ) );
            dic.Add( 0x1fd, "UHJvZ3JhbSBoYXMgYmVlbiByZWdpc3RlcmVkIHN1Y2Nlc2Z1bGx5IQ==" );
            dic.Add( 0x237, "Crackme" );
            dic.Add( 0x240, "unknown asm" );
            dic.Add( 0x256, typeof( MessageBox ).GetMethod( "Show", BindingFlags.Static | BindingFlags.Public, null, new Type[] { typeof( string ), typeof( string ) }, null ) );

            return dic;
        }

        static void DelegateCleaner ( )
        {
            var file = @"Z:\4\3\Crackme\Crackme_renamed.exe";
            var bak = @"Z:\4\3\Crackme\Crackme.exe.bak_dc";

            var t = Type.GetTypeFromHandle( typeof( string ).TypeHandle ).Name;

            File.Copy( file, bak, true );

            using ( var m = ModuleDefMD.Load( bak ) )
            {
                DelegateKiller.Kill( m );

                m.Write( bak + ".exe" );
            }
        }

        static void RunMethodCleaner ( )
        {
            using ( var p = new CodeDecrypter( @"Z:\4\3\Crackme\Crackme.exe" ) )
            {
                //p.patch( 0xBB8 ); // FormDispose
                //p.patch( 0xB0C ); // FormMethod29
                //p.patch( 0xAE8 ); // FormMethod28
                //p.patch( 0xC00 ); // FormInitializeComponent
                //p.patch( 0x330 ); // click - down stack 0
                //p.patch( 0x60C ); // click - down stack 1
                //p.patch( 0x1190 ); // property type of byte[]
                // FAIL: p.patch( 0x298 ); // resource decrypter (token - E), placed on button click
                //p.patch( 0x37C );
                p.Clean( 0x574 );
            }
        }

        private static string ToHex ( byte[] buff )
        {
            var sb = new StringBuilder();

            for ( int i = 0; i < buff.Length; i++ )
            {
                sb.AppendFormat( "{0:X2}", buff[i] );
            }

            return sb.ToString();
        }
    }
}
